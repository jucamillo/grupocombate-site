<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package combate
 */

get_header();
?>


<section class="top" style="background-image:url(<?php the_field('imagem_topo_blog', 'option'); ?>);">
	<section class="bottom">
		<div class="container">
			<h1>Blog</h1>
		</div>
	</section>
</section>

<section>
	<div class="container">
		<div class="row">
			
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>

		</div>
	</div>
</section>

<?php
get_footer();
