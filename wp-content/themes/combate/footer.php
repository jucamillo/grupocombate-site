<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package combate
 */

?>
	</section>

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="col-xs-12">
                <?php if( get_field('copyright_rodape', 'option') ): ?>
					<span>
						<?php the_field('copyright_rodape', 'option'); ?>
						
					</span>
                <?php endif; ?>

				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-2',
				) );
				?>

				<a href="https://qualitare.com.br" class="feito" target="_blank">
					<div class="size">
						<img src="<?php echo get_template_directory_uri(); ?>/images/qualitare.svg">
					</div>
				</a>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
	

jQuery(function(){
    var hasBeenTrigged = false;
    jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
             jQuery('header').addClass('scroll');
            hasBeenTrigged = true;
        } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
             jQuery('header').removeClass('scroll');
            hasBeenTrigged = false;

        }
    });



    var scrollServ = false;

    jQuery(window).scroll(function() {
        var top_of_element = jQuery("#servicos-row").offset().top;
        var bottom_of_element = jQuery("#servicos-row").offset().top + jQuery("#servicos-row").outerHeight();
        var bottom_of_screen = jQuery(window).scrollTop() + jQuery(window).innerHeight();
        var top_of_screen = jQuery(window).scrollTop();

        if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)){
            
            if (!scrollServ) { 
                open1();
                console.log('a');
                scrollServ = true;
            }
        }
    });
    
    jQuery('select').select2();
});



jQuery('#banner-home ul.wpb_image_grid_ul').addClass('owl-carousel');



jQuery('.vc_btn3-container.whatsapp > *').prepend('<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M3.02644 13.4803L3.69726 13.8157L3.90474 13.4008L3.62608 13.0298L3.02644 13.4803ZM4.51955 14.9734L4.97003 14.3738L4.5991 14.0951L4.18414 14.3026L4.51955 14.9734ZM1.53333 16.4665L0.862505 16.1311C0.718134 16.4199 0.774725 16.7686 1.003 16.9969C1.23127 17.2251 1.57999 17.2817 1.86874 17.1374L1.53333 16.4665ZM5.79999 5.79987L5.38397 5.17583C5.17532 5.31493 5.04999 5.5491 5.04999 5.79987H5.79999ZM12.2 12.1999V12.9499C12.4508 12.9499 12.6849 12.8245 12.824 12.6159L12.2 12.1999ZM7.6712 6.36045L8.41099 6.23715L7.6712 6.36045ZM7.81994 7.25289L7.08014 7.37619H7.08014L7.81994 7.25289ZM7.35947 8.31577L7.77549 8.93981V8.93981L7.35947 8.31577ZM12.3516 11.9725L12.9756 12.3885L12.3516 11.9725ZM11.6394 10.3287L11.7627 9.58887L11.6394 10.3287ZM10.747 10.1799L10.6237 10.9197L10.747 10.1799ZM9.68408 10.6404L9.06005 10.2244L9.68408 10.6404ZM0.783325 8.99987C0.783325 10.849 1.39502 12.5574 2.4268 13.9308L3.62608 13.0298C2.78276 11.9073 2.28333 10.513 2.28333 8.99987H0.783325ZM8.99999 0.783203C4.46205 0.783203 0.783325 4.46193 0.783325 8.99987H2.28333C2.28333 5.29036 5.29048 2.2832 8.99999 2.2832V0.783203ZM17.2167 8.99987C17.2167 4.46193 13.5379 0.783203 8.99999 0.783203V2.2832C12.7095 2.2832 15.7167 5.29036 15.7167 8.99987H17.2167ZM8.99999 17.2165C13.5379 17.2165 17.2167 13.5378 17.2167 8.99987H15.7167C15.7167 12.7094 12.7095 15.7165 8.99999 15.7165V17.2165ZM4.06908 15.5731C5.4425 16.6048 7.15089 17.2165 8.99999 17.2165V15.7165C7.48689 15.7165 6.0926 15.2171 4.97003 14.3738L4.06908 15.5731ZM1.86874 17.1374L4.85496 15.6442L4.18414 14.3026L1.19791 15.7957L1.86874 17.1374ZM2.35562 13.1449L0.862505 16.1311L2.20415 16.8019L3.69726 13.8157L2.35562 13.1449ZM5.04999 5.79987V6.86654H6.54999V5.79987H5.04999ZM11.1333 12.9499H12.2V11.4499H11.1333V12.9499ZM5.04999 6.86654C5.04999 10.2263 7.77359 12.9499 11.1333 12.9499V11.4499C8.60202 11.4499 6.54999 9.39784 6.54999 6.86654H5.04999ZM6.21602 6.42391L6.44339 6.27232L5.61134 5.02425L5.38397 5.17583L6.21602 6.42391ZM6.9314 6.48375L7.08014 7.37619L8.55974 7.1296L8.41099 6.23715L6.9314 6.48375ZM6.94344 7.69174L5.9173 8.37583L6.74935 9.62391L7.77549 8.93981L6.94344 7.69174ZM7.08014 7.37619C7.10063 7.49911 7.04712 7.62261 6.94344 7.69174L7.77549 8.93981C8.37031 8.54327 8.67726 7.83475 8.55974 7.1296L7.08014 7.37619ZM6.44339 6.27232C6.63459 6.14486 6.89363 6.25708 6.9314 6.48375L8.41099 6.23715C8.19427 4.93681 6.70821 4.293 5.61134 5.02425L6.44339 6.27232ZM12.824 12.6159L12.9756 12.3885L11.7275 11.5565L11.576 11.7838L12.824 12.6159ZM11.7627 9.58887L10.8703 9.44012L10.6237 10.9197L11.5161 11.0685L11.7627 9.58887ZM9.06005 10.2244L8.37595 11.2505L9.62403 12.0826L10.3081 11.0564L9.06005 10.2244ZM10.8703 9.44012C10.1651 9.3226 9.45659 9.62955 9.06005 10.2244L10.3081 11.0564C10.3772 10.9527 10.5007 10.8992 10.6237 10.9197L10.8703 9.44012ZM12.9756 12.3885C13.7069 11.2916 13.0631 9.80559 11.7627 9.58887L11.5161 11.0685C11.7428 11.1062 11.855 11.3653 11.7275 11.5565L12.9756 12.3885Z" fill="#04144C"/></svg>');


jQuery('.vc_btn3-container.mail > *').prepend('<svg width="17" height="14" viewBox="0 0 17 14" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.5 4L8.5 8L15.5 4M2.5 1H14.5C15.0523 1 15.5 1.44772 15.5 2V12C15.5 12.5523 15.0523 13 14.5 13H2.5C1.94772 13 1.5 12.5523 1.5 12V2C1.5 1.44772 1.94772 1 2.5 1Z" stroke="black" stroke-width="1.5"/></svg>');



jQuery('#banner-home ul.wpb_image_grid_ul').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: false,
    loop: true,
    nav:false,
    autoHeight:true,
    autoplay: true,
    autoplayTimeout: 10000,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768:{
            items:1
        },
        1200:{
            items:1
        }
    }
})


jQuery('ul.servicos > li:first-child').addClass('active');


jQuery('ul.servicos-img').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: false,
    loop: true,
    nav:false,
    autoHeight:false,
    autoplay: false,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768:{
            items:1
        },
        1200:{
            items:1
        }
    }
})
function open1(){

        jQuery('ul.servicos > li').removeClass('active');
        jQuery('ul.servicos > li:nth-child(1)').addClass('active');
        jQuery('ul.servicos-img').trigger('to.owl.carousel', 6);
        myVar = setTimeout( function() {
            open2();
        }, 10000);


}

function open2(){

        jQuery('ul.servicos > li').removeClass('active');
        jQuery('ul.servicos > li:nth-child(2)').addClass('active');
        jQuery('ul.servicos-img').trigger('to.owl.carousel', 1);
        myVar = setTimeout( function() {
            open3();
        }, 10000);

}
function open3(){
        jQuery('ul.servicos > li').removeClass('active');
        jQuery('ul.servicos > li:nth-child(3)').addClass('active');
        jQuery('ul.servicos-img').trigger('to.owl.carousel', 2);
        myVar = setTimeout( function() {
            open4();
        }, 10000);

}
function open4(){
        jQuery('ul.servicos > li').removeClass('active');
        jQuery('ul.servicos > li:nth-child(4)').addClass('active');
        jQuery('ul.servicos-img').trigger('to.owl.carousel', 3);
        myVar = setTimeout( function() {
            open5();
        }, 10000);

}
function open5(){
        jQuery('ul.servicos > li').removeClass('active');
        jQuery('ul.servicos > li:nth-child(5)').addClass('active');
        jQuery('ul.servicos-img').trigger('to.owl.carousel', 4);
        myVar = setTimeout( function() {
            open6();
        }, 10000);

}
function open6(){
        jQuery('ul.servicos > li').removeClass('active');
        jQuery('ul.servicos > li:nth-child(6)').addClass('active');
        jQuery('ul.servicos-img').trigger('to.owl.carousel', 5);
        myVar = setTimeout( function() {
            open1();
        }, 10000);
}








jQuery(document).delegate('ul.servicos > li h3 a', 'click', function(event) {
    event.preventDefault();


    var link = jQuery(this).attr('href');
    var values = link.split('um');
    var pag = values[0];
    var num = values[1];

    clearTimeout(myVar);


    if(num == "1"){
        open1();
    }
    if(num == "2"){
        open2();
        
    }
    if(num == "3"){
        open3();
    }
    if(num == "4"){
        open4();
        
    }
    if(num == "5"){
        open5();
        
    }
    if(num == "6"){
        open6();
        
    }

    /*
    jQuery('ul.servicos > li').removeClass('active');
    jQuery('ul.servicos').addClass('stop');
    jQuery(this).parent().parent().addClass('active'); */
});



jQuery(document).delegate('header nav.main-navigation .menu-content button.close-btn', 'click', function(event) {
    event.preventDefault();
    jQuery( "header nav.main-navigation .menu-toggle" ).trigger( "click" );

});


jQuery('ul.depoimentos').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    loop: true,
    nav:false,
    autoHeight:true,
    autoplay: true,
    autoplayTimeout: 10000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        768:{
            items:1
        },
        1200:{
            items:1
        }
    }
})



jQuery(document).delegate('body.home #menu-item-41 > a', 'click', function(event) {

    event.preventDefault();
    var link = jQuery(this).attr('href');
    var values = link.split('#');
    var pag = values[0];
    var tab = '#' + values[1];
    var divPosition = jQuery(tab).offset();
    var scrollPosition = divPosition.top ;
    jQuery('html, body').animate({scrollTop: scrollPosition}, 400);
    console.log(tab);


    jQuery('header  .main-navigation').removeClass('toggled');


});



jQuery('.page-contato select').on('change', function() {
    jQuery('.vc_row.cidade').removeClass('active');


    jQuery('.vc_row.cidade#' + this.value).addClass('active');
});


if (jQuery(window).width() < 992) {
jQuery('.vc_row#valores').addClass('owl-carousel');

setTimeout(
  function() 
  {
    jQuery('.vc_row#valores').owlCarousel({
        loop:false,
        margin:0,
        responsiveClass:true,
        dots: false,
        nav:false,
        autoHeight:false,
        autoplay: false,
        autoplayTimeout: 10000,
        responsive:{
            0:{
                items:1,
                stagePadding:60,
            },
            530:{
                items:1,
                stagePadding:140,
            },
            768:{
                items:2,
                stagePadding:140,
            },
            1200:{
                items:5
            }
        }
    })
  }, 1000);

}  
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(11)').addClass('b1');

    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(1)').addClass('b2');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(2)').addClass('b1');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(3)').addClass('b3');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(4)').addClass('left');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(5)').addClass('b1');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(6)').addClass('down');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(7)').addClass('b3');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(8)').addClass('down');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(9)').addClass('right');
    jQuery('.free-img ul.wpb_image_grid_ul li:nth-child(10)').addClass('b2');










var left = {
    delay: 300,
    distance: '100px',
    duration: 1300,
    origin: 'left',
    easing: 'ease-in-out',
};


var right = {
    delay: 300,
    distance: '100px',
    duration: 1300,
    origin: 'right',
    easing: 'ease-in-out',
};





var up1 = {
    delay: 100,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up2 = {
    delay: 400,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var up3 = {
    delay: 700,
    distance: '100px',
    duration: 1300,
    origin: 'bottom',
    easing: 'ease-in-out',
};




var down = {
    delay: 100,
    distance: '100px',
    duration: 1000,
    origin: 'top',
    easing: 'ease-in-out',
};


var down2 = {
    delay: 400,
    distance: '100px',
    duration: 1000,
    origin: 'top',
    easing: 'ease-in-out',
};






var b1 = {
    delay: 0,
    distance: '200px',
    scale: 0.85,
    duration: 500,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var b2 = {
    delay: 200,
    distance: '200px',
    scale: 0.65,
    duration: 700,
    origin: 'bottom',
    easing: 'ease-in-out',
};

var b3 = {
    delay: 400,
    distance: '200px',
    scale: 0.55,
    duration: 1000,
    origin: 'bottom',
    easing: 'ease-in-out',
};



jQuery(document).ready(function(){



    ScrollReveal().reveal('.up1', up1);
    ScrollReveal().reveal('.up2', up2);
    ScrollReveal().reveal('.up3', up3);
    ScrollReveal().reveal('.b1', b1);
    ScrollReveal().reveal('.b2', b2);
    ScrollReveal().reveal('.b3', b3);
    ScrollReveal().reveal('.left', left);
    ScrollReveal().reveal('.right', right);
    ScrollReveal().reveal('.down', down);
    ScrollReveal().reveal('.down2', down2);





});






</script>
</body>
</html>
