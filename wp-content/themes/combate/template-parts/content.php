<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package combate
 */

    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );

    if ( !is_singular() ) :
    	echo '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 post-latest"><a href="'.get_the_permalink().'" class="img" style="background-image:url('.$image[0].');"></a>'; ?>
	        <h3>
	            <?php the_title(); ?>
	        </h3>
	        <div class="info">
	            <span class="date">
	                <?php echo get_the_date(); ?>             
	            </span>
	            <ul class="blog-categories">
	              <?php
	                //get all the categories the post belongs to
	                $categories = wp_get_post_categories( get_the_ID() );
	                //loop through them
	                foreach($categories as $c){
	                  $cat = get_category( $c );
	                  //get the name of the category
	                  $cat_id = get_cat_ID( $cat->name );
	                  //make a list item containing a link to the category
	                  echo '<li><a href="'.get_category_link($cat_id).'" title="'.$cat->name.'">'.$cat->name.'</a></li>';
	                }
	              ?>
	            </ul>            
	        </div>
	        <p>
	            <?php echo strip_tags( get_the_excerpt() ); ?>
	        </p>
	        <a href="<?php echo get_the_permalink(); ?>" class="btn">
	        	Saiba Mais
	        </a>
        </div>





		<?php else : ?>
			<section class="single-blog">
				<div class="container">
					<div class="row">
						<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
							
						</div>
						<div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
							


						<h1><?php the_title(); ?></h1>


						<?php
						combate_post_thumbnail();
						echo '<div class="content-info-noticias">';
						the_content( );
						echo '</div> ';

			 			?>
							<div class="compartilhe">
								<h3>Compartilhe</h3><?php echo do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS]'); ?>
							</div>
							<div class="fb-comments" data-href="<?php echo get_permalink(); ?>" data-width="100%" data-numposts="8"></div>

							<div class="read">
								<h2>Leitores também acessaram:</h2>
				 				<div class="row"><?php echo do_shortcode( '[my_related_posts]' ); ?>	</div>
							</div>


						</div>
						<div class="col-lg-2 col-md-1 col-sm-12 col-xs-12">
							
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>