<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package combate
 */

get_header();
?>

<section class="top" style="background-image:url(<?php the_field('imagem_topo_blog', 'option'); ?>);">
	<section class="bottom">
		<div class="container">
			<h1><?php the_archive_title(); ?></h1>
		</div>
	</section>
</section>

<section>
	<div class="container">
		<div class="row">
			
			<?php
			if ( have_posts() ) :
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', get_post_type() );

				endwhile;
				wpbeginner_numeric_posts_nav();

			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>

		</div>
	</div>
</section>

<?php
//get_sidebar();
get_footer();
