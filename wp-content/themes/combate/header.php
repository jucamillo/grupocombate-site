<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package combate
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<meta name="description" content="Atuamos com grande diversidade de serviços e oferecemos projetos e soluções customizadas de segurança para o setor público e privado, residências e condomínios.">
	<meta name="keywords" content="segurança pessoal e privada, portaria virtual, facilities, terceirização, rastreamento veicular, escolta armada, segurança eletrônica.">
	<meta name="robots" content="">
	<meta name="revisit-after" content="1 day">
	<meta name="language" content="Portuguese">
	<meta name="generator" content="N/A">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

	<?php wp_head(); ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,400;0,600;0,700;1,400&family=PT+Serif&display=swap" rel="stylesheet">
	
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
	
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.css" integrity="sha512-phGxLIsvHFArdI7IyLjv14dchvbVkEDaH95efvAae/y2exeWBQCQDpNFbOTdV1p4/pIa/XtbuDCnfhDEIXhvGQ==" crossorigin="anonymous" />
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


    <script src="https://unpkg.com/scrollreveal@4"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Pular para o conteúdo', 'combate' ); ?></a>

	<header class="site-header">
		<div class="container">
			<div class="col-xs-12">
				<div class="border">
						
					<nav id="site-navigation" class="main-navigation">
						<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
							<img src="<?php echo get_template_directory_uri(); ?>/images/menu.svg">
							<span>
								Menu
							</span>
						</button>
						<div class="menu-content">
							<button class="close-btn">					
								<img src="<?php echo get_template_directory_uri(); ?>/images/close.svg">
							</button>
							<img src="<?php echo get_template_directory_uri(); ?>/images/shield.svg" class="shield">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'menu-1',
								'menu_id'        => 'primary-menu',
							) );
							?>

							<div class="bottom">
			                    <?php if( have_rows('redes_sociais', 'option') ): ?>
			                        <ul class="redes_sociais">
			                        <?php while( have_rows('redes_sociais', 'option') ): the_row();
			                            ?>
			                            <li>
			                                <a href="<?php the_sub_field('link'); ?>" target="_blank">
			                                    <?php the_sub_field('icone'); ?>
			                                </a>
			                            </li>
			                        <?php endwhile; ?>
			                        </ul>
			                    <?php endif; ?>
								
		                        <?php if( get_field('copyright', 'option') ): ?>
									<span>
										<?php the_field('copyright', 'option'); ?>
										
									</span>
			                    <?php endif; ?>
							</div>
						</div>
					</nav> 
						
					<div class="branding">
						<?php
						the_custom_logo();
						?>
					</div>

					<?php if( have_rows('botao_orcamento', 'option') ): ?>
					    <?php while( have_rows('botao_orcamento', 'option') ): the_row(); 
					        ?>
					        <a href="<?php the_sub_field('link'); ?>" class="btn">
					        	<?php the_sub_field('texto'); ?>
					        </a>
					    <?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->


	<section id="content" class="miolo-site">