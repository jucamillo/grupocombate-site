<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package combate
 */

get_header();
?>

<section class="top" style="background-image:url(<?php the_field('imagem_topo_blog', 'option'); ?>);">
	<section class="bottom">
		<div class="container">
			<h1>Oops, página não encontrada!</h1>
		</div>
	</section>
</section>
<section>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<p>Parece que a página que você está procurando não existe ou foi removida. Tente uma busca!</p>
				<?php get_search_form(); ?>
			</div>
		</div>
	</div>
</section>

<?php
get_footer();
